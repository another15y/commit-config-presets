# Architecture Project Template

> The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", 
> "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", 
> "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document 
> are to be interpreted as described in BCP 14 [RFC2119](https://www.rfc-editor.org/rfc/rfc2119) [RFC8174](https://www.rfc-editor.org/rfc/rfc8174.txt) when, 
> and only when, they appear in all capitals, as shown here.

This project is the template for many Architecture projects.

The following sections describe common features that should be reviewed on the initial selection and use of the template.

## Prerequisites

To get the most out of the template, ensure that Node is available. Any LTS version should be supported.

## Begin

Run the following command:

```shell
npm install
```

The `package.json` supports Husky and `is-cd`. Husky provides Git hooks support for the validation of commit messages. `is-cd` "guards" the use of Husky in GitLab CI/CD pipelines: automated pipeline commits may not, but often should, follow Conventional Commits.

The `package.json` declares a `prepare` script. This [lifecycle run script of NPM](https://docs.npmjs.com/cli/v10/using-npm/scripts#life-cycle-scripts) is automatically performed in `npm install`. The script calls the custom `husky:install`  run script that will locally set Git hooks (see [`core.hookspath`](https://git-scm.com/docs/githooks)) providing Conventional Commit validation in the cloned repository.

## Features

### To Be Continuous (TBC) Semantic Release

The template leverages [TBC Semantic Release](https://to-be-continuous.gitlab.io/doc/ref/semantic-release/) to version and tag the project.

The TBC template leverages several advanced configuration features that must be considered in the new project. These features have application across all target state projects leveraging Conventional Commits.

**Notable Files**

- `.releaserc.yml`
- `semrel-required-plugins.txt`
-  `CHANGELOG.md`

### `.releaserc.yml`

This file replaces the "internally generated" file provided by TBC. When present, TBC load the file and configure Semantic Release.

The file provides specific configurations to the `commit-analyzer`, `npm`, and `changelog` plugins.

#### `commit-analyzer`

Sets the Commit Analyzer to use the Conventional Commits specification in analyzing commits for changes. The default analyzer follows the `angular` commit specification.

#### `npm`

Sets the `npmPublish` to `false`. This MUST be reviewed by project and would typically default to `true` (when not set). 

#### `changelog`

Overriding TBC's default config means losing some of the defaults for the `changelog` plugin. The provided configuration restores the TBC defaults and overrides the CHANGELOG header when the `CHANGELOG.md` file is written.

### CODEOWNERS

CODEOWNERS is used by GitLab to provide review and approval gates for key files and directories in a project. For example, it is possible to require specific user approvals for changes to `CODEOWNERS` (self) or `.gitlab-ci.yml`.

For more information on the uses of CODEOWNERS, see [Code Owners](https://docs.gitlab.com/ee/user/project/code_owners.html).

The provided `CODEOWNERS` file is empty.

**Notable Files**
- `CODEOWNERS`

### node

The project template includes Node to enable the use of [Husky](https://typicode.github.io/husky/#/). Husky enables the use of `commitlint` and `semantic-release` in GitLab pipelines.

Projects SHOULD update the `package.json` to reflect the project metadata.

**Notable Files**
- `package.json`
- `package-lock.json`

### commitlint

[commitlint](https://commitlint.js.org/#/) is a Node tool executed via Husky's Git hook `commit-msg` to provide a quality gate to all commits. Commits are checked against the [Conventional Commits Specification](https://www.conventionalcommits.org/en/v1.0.0/). Commits are rejected if incomplete or incorrectly formatted. A functional plugin is provided to extend `commitlint` scope parsing and supports Jira story IDs.

**Notable Files**
- `commitlint.config.js`

### Issue and MR templates

The project includes issue and merge request templates. These templates help issue and feature reporters with the creation of the same and ensure consistency in resolutions. The templates make extensive use of project labels in the Architecture group.

**Notable files**
- `.gitlab/issue_templates/default.md`
- `.gitlab/merge_request_templates/default.md`

### Contribution Guidelines

A `CONTRIBUTING.md` file is provided. On initial project creation the file should be updated to the specific URL and point the correct project resources and pages.

### Icon

GitLab supports the use of _portable_ project icons. It is possible to set the project icon in the UI, however, this preference is not copied to forks. A `logo.png`, or JPG, can be committed to the project and provides a consistent, source code controlled, and portable branding element.
