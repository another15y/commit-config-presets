module.exports = {
  'extends': ['@commitlint/config-conventional'],
  'plugins': ['commitlint-plugin-function-rules'],
  'rules': {
    'scope-enum': [0],
    'function-rules/scope-enum': [1, 'always', (parsed) => {
        const scopes = ['[A-Z]{2,5}-', 'checkstyle', 'config', 'gitlab', 'husky', 'maven', 'docker', 'helm'];

        if (!parsed.scope || scopes.some( (scope) => new RegExp('^' + scope).test(parsed.scope) )) {
          return [true];
        }
        return [false, 'scope SHOULD match (by regular expression) one of\n    ' + scopes.join(', ')];
      },
    ],
  },
};
