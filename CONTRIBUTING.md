# Contributing

Firstly, thanks for your interest in contributing! We hope that this will be a
pleasant experience for you, and that you will return to continue contributing.

## How Can I Contribute?

Most of the contributions that we receive are code contributions, but you can
also contribute to the documentation, wiki, etc., or simply report solid bugs
for us to fix.

### Reporting Issues

Issues should be opened on [GitLab](https://gitlab.com/another15y/templates/common-project-template/-/issues). Issues are automatically marked for triage and the following sprint.

### Your First Code Contribution

### Trouble Deciding How to Contribute?

Unsure where to begin contributing? You can start by taking a look at
the issues marked 'beginner'. Please note that the Beginner keyword
is pretty new to the project, so if there aren't any issues in the filter feel
free to ask on via email or Teams.

The list above shows all bugs that are marked 'beginner' and are open in the
currently supported releases.

### How to Provide Your First Patch

Excited yet? This section will guide you through providing a patch to the
committers of the project for review and acceptance.

#### Choose Your Method of Submission

You can provide a patch in one of the following ways (in order of preference):

* GitLab Merge Request (recommended)
* Patch attachment to the GitLab issue
* Email the patch to the maintainers or other committers for review (least preferred)

### Get the Sources

Now that you've chosen how you want to submit a patch, you need to get the
source code.

#### Download The Source Distribution

This method works if you want to submit a patch via email (least preferred method), but
the difference in using the sources distribution and GitLab is that you have to
manually generate the patch file by using `diff`. If this is what you want, you
can download the sources from GitLab project homepage using the download button, :inbox-tray:. 

##### Manual Patch Generation

If you have chosen to attach a patch to the GitLab issue (or email
one), then you'll need to download the sources as noted above, make your
desired changes and then manually generate your patch using `diff` (or any
other tool).

##### GitLab Merge Request

To submit a GitLab Merge Request you'll need to fork the
[repository](https://gitlab.com/another15y/templates/common-project-template), then clone your fork to do the work:


```shell
$ git clone git@gitlab.com:another15y/templates/commonproject-template.git
```

Push your changes and submit a Merge Request via the GitLab UI.

#### Submitting Your Patch!

After you've chosen your method of submission, retrieved the sources, and
fixed the issue it's time to submit your work. At this point, just follow
the method of submission you chose earlier.

* GitLab Merge Request - after resolving the issue in your local fork and pushing to your
copy of the repository, open a GitLab Merge Request for review.
* GitLab Issue attachment - attach the patch to the GitLab Issue issue
* Email - again, not preferred, but you may send an email to the developer list
with a patch attached for review.

#### Waiting For Feedback

It may take a while for committers to review. Please be patient during this
time as all committers work on projects committed to in a sprint. If a significant amount
of time has lapsed since your submission, such as a couple of weeks, feel free
to either update your Issue, Merge Request, or email with a message to bump your
issue. Sometimes things get lost in all the work a reminder is welcome. :smile:

### Style Guide

* Use spaces for indenting, not tabs
* 120 char line width for Java source, 80 char line width for documentation
source (.md, .txt, .xml)
* Java source: `{` at end of line, 2 space indents
* XML source: 2 space indents

## Did we miss something?

Have you reviewed this guide and found it lacking? Or are you confused about
some particular step? If so, please let us know! Or better yet, open an issue and submit a Merge Request to
address the issue :wink:
